package hnweb.com.ayurvedaapp;

/**
 * Created by Hnweb on 9/23/2016.
 */

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.error.VolleyError;
import com.android.volley.request.SimpleMultiPartRequest;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

import me.nereo.multi_image_selector.MultiImageSelector;


public class Activity_save_data extends Activity implements ImageChooserListener, View.OnFocusChangeListener, TextWatcher, View.OnClickListener {
    ListView timeLV;
    GridView gv;
    TextView tv_new;
    private static final int REQUEST_IMAGE = 2;
    protected static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101;
    protected static final int REQUEST_STORAGE_WRITE_ACCESS_PERMISSION = 102;

    private TextView mResultText, mResultText2, mResultText3;
    private RadioGroup mChoiceMode, mShowCamera;
    private EditText mRequestNum;

    private ArrayList<String> mSelectPath;

    private ArrayList<String> mSymSelectPath;

    AutoCompleteTextView textIn;
    Button buttonAdd;
    LinearLayout container;
    String appendd = "";
    AutoCompleteTextView childTextView;
    private static final String[] NUMBER = new String[]{
            "One", "Two", "Three", "Four", "Five",
            "Six", "Seven", "Eight", "Nine", "Ten"
    };
    ArrayAdapter<String> adapter;

    ProgressDialog pdialog;
    String isLogin;
    Button submit, upload_image_button, upload_docs, select_package;
    EditText et_name, et_email_id, et_symptom1, et_details, et_additional_details, et_age;
    RadioGroup rg_gender;
    String name, email, symptom1, symptom2, symptom3, details, image, res, payment_id;
    ImageView imageviewpropic;
    private ImageChooserManager imageChooserManager;
    private String filePath;
    private int chooserType;
    String img_str, customer_id, full_name, email_address;
    ProgressDialog progressDialog;
    AutoCompleteTextView textOut;
    TextInputLayout til_name, til_email, til_age, til_symptoms, til_details, til_add_details;
    SharedPreferences settings;
    /////////////////////// web service to save data ////////////////////////
    ///buy views

    private static final String SAVE_DATA_URL = "http://designer321.com/kaushaljohn/ayurvedic/api/save_customer_details.php";


    public static final String KEY_CUSTOMER_ID = "customer_id";

    /// public static final String KEY_FULLNAME = "name";
    public static final String KEY_NAME = "name";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_GENDER = "gender";
    public static final String KEY_AGE = "age";
    public static final String KEY_SYMPTOM1 = "symptom1";
    public static final String KEY_SYMPTOM_PIC = "picture_of_symptom";
    public static final String KEY_DETAILS = "details";
    public static final String KEY_DEVICE_TYPE = "device_type";
    public static final String KEY_ADDITIONAL_DETAILS = "additional_details";
    public static final String KEY_PICTURE_FILE = "picture_file";

    public static final String KEY_MEDICAL_DOC = "medical_document_file";
    public static final String KEY_VIDEO_FILE = "video_file";

    public static final String KEY_PACKAGE_NAME = "package_name";
    public static final String KEY_AMOUNT_PAID = "amount_paid";
    public static final String KEY_PAYMENT_TXN_ID = "payment_txn_id";

    AllPackages allPackages;
    ProgressDialog pd;
    ImageView nameicon, emailicon, ageicon, symptomicon, detailsicon, adddetailsicon, addsymptomsicon;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    private static boolean doubleBackToExitPressedOnce = false;
    ArrayList<AllPackages> allPackagesArrayList = new ArrayList<AllPackages>();

    /////////////////////////////////////////////////////////////////////////


    @Override
    public void onBackPressed() {
//        super.onBackPressed();

        if (doubleBackToExitPressedOnce) {
//            super.onBackPressed();

            this.finish();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click back again to exit from Ayurvedic App", Toast.LENGTH_SHORT)
                .show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_data);

        gv = (GridView) findViewById(R.id.gridView1);
        pdialog = new ProgressDialog(this);
        pd = new ProgressDialog(Activity_save_data.this);

        til_name = (TextInputLayout) findViewById(R.id.input_layout_name);
        til_email = (TextInputLayout) findViewById(R.id.input_layout_email);
        til_age = (TextInputLayout) findViewById(R.id.input_layout_age);
        til_symptoms = (TextInputLayout) findViewById(R.id.input_layout_symptoms);
        til_details = (TextInputLayout) findViewById(R.id.input_layout_details);
        til_add_details = (TextInputLayout) findViewById(R.id.input_layout_additional_details);

//        nameicon = (ImageView) findViewById(R.id.nameicon);
//        emailicon = (ImageView) findViewById(R.id.emailicon);
//        ageicon = (ImageView) findViewById(R.id.ageicon);
//        symptomicon = (ImageView) findViewById(R.id.symptomicon);
//        detailsicon = (ImageView) findViewById(R.id.detailsicon);
//        adddetailsicon = (ImageView) findViewById(R.id.adddetailsicon);


        mResultText = (TextView) findViewById(R.id.result);
        mResultText2 = (TextView) findViewById(R.id.result1);
        mResultText3 = (TextView) findViewById(R.id.result2);
//        tv_new = (TextView) findViewById(R.id.new_tv);
        Typeface type = Typeface.createFromAsset(getAssets(), "fonts/roboto.regular.ttf");
//        tv_new.setTypeface(type);
        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, NUMBER);

        textIn = (AutoCompleteTextView) findViewById(R.id.textin);
        textIn.setAdapter(adapter);

        buttonAdd = (Button) findViewById(R.id.add);
        container = (LinearLayout) findViewById(R.id.container);

        settings = PreferenceManager.getDefaultSharedPreferences(Activity_save_data.this);

        rg_gender = (RadioGroup) findViewById(R.id.radioGroup1);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //  setSupportActionBar(toolbar);
        progressDialog = new ProgressDialog(Activity_save_data.this);
//        logout = (Button) findViewById(R.id.logout_button);
        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        customer_id = "0";

        full_name = settings.getString("full_name", "");
        email_address = settings.getString("email_address", "");
        LinearLayout l1 = (LinearLayout) findViewById(R.id.ll1);

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflater =
                        (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View addView = layoutInflater.inflate(R.layout.layout_row, null);
                textOut = (AutoCompleteTextView) addView.findViewById(R.id.textout);
//                addsymptomsicon = (ImageView) addView.findViewById(R.id.addsymptomsicon);
                textOut.setAdapter(adapter);
                textOut.setOnFocusChangeListener(Activity_save_data.this);
                // textOut.setText(textIn.getText().toString());
                Button buttonRemove = (Button) addView.findViewById(R.id.remove);

                final View.OnClickListener thisListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        ((LinearLayout) addView.getParent()).removeView(addView);
                        listAllAddView();
                    }
                };

                buttonRemove.setOnClickListener(thisListener);
                container.addView(addView);
                listAllAddView();
            }
        });

//        logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //  settings = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
//
//                AlertDialog.Builder builder = new AlertDialog.Builder(Activity_save_data.this);
//                builder.setCancelable(false);
//                builder.setTitle("Logout");
//                builder.setMessage("Are you sure you want to logout?");
//                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        AppConstants.logout_flag = "1";
//
//                        AppConstants.cname = "";
//                        AppConstants.email = "";
//                        AppConstants.symptom1 = "";
//                        AppConstants.symptom2 = "";
//                        AppConstants.symptom3 = "";
//                        AppConstants.details = "";
//                        AppConstants.additional_details = "";
//                        AppConstants.img = "";
//                        AppConstants.plan = "";
//                        AppConstants.price = "";
//                        AppConstants.plan_id = "";
//                        AppConstants.gender = "";
//                        AppConstants.age = "";
//                        SharedPreferences.Editor editor = settings.edit();
//
//                        isLogin = "";
//                        editor.putString("isLogin", isLogin);
//                        editor.commit();
//
//                        Intent intent = new Intent(Activity_save_data.this, Activity_login.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(intent);
//                        finishAffinity();
//                    }
//                });
//                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                    }
//                });
//                builder.show();
//
//
//            }
//        });


        et_age = (EditText) findViewById(R.id.activity_login_edittext_age);
        //      setSupportActionBar(toolbar);
        et_name = (EditText) findViewById(R.id.activity_registration_edittext_full_name);
        et_name.setText(full_name);

        et_email_id = (EditText) findViewById(R.id.activity_login_edittext_email_id);
        et_email_id.setText(email_address);

        et_symptom1 = (EditText) findViewById(R.id.activity_login_edittext_password);
        //   et_symptom2=(EditText)findViewById(R.id.activity_info_edittext_symptom2);
        //   et_symptom3=(EditText)findViewById(R.id.activity_info_edittext_symptom3);
        et_details = (EditText) findViewById(R.id.activity_info_edittext_details);
        et_additional_details = (EditText) findViewById(R.id.activity_info_edittext_additional_details);

        imageviewpropic = (ImageView) findViewById(R.id.activity_info_imageview_upload_image);
        submit = (Button) findViewById(R.id.activity_registration_button_submit);
        upload_docs = (Button) findViewById(R.id.activity_info_button_upload_image1);
        select_package = (Button) findViewById(R.id.activity_info_button_select_package);

        et_age.addTextChangedListener(this);
        et_name.addTextChangedListener(this);
        et_email_id.addTextChangedListener(this);
        textIn.addTextChangedListener(this);
        et_details.addTextChangedListener(this);
        et_additional_details.addTextChangedListener(this);

        et_age.setOnFocusChangeListener(this);
        et_name.setOnFocusChangeListener(this);
        et_email_id.setOnFocusChangeListener(this);
        textIn.setOnFocusChangeListener(this);
        et_details.setOnFocusChangeListener(this);
        et_additional_details.setOnFocusChangeListener(this);

        upload_docs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AppConstants.report_or_symptoms = "report";
                pickImage();

            }
        });

        select_package.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selectPackage();
            }
        });

        upload_image_button = (Button) findViewById(R.id.activity_info_button_upload_image);
        Intent intent = getIntent();

        String fName = intent.getStringExtra("pay_success");
        // String value = getIntent().getExtras().getString("pay_success");
        if (fName != null) {
            //System.out.println(""+fName);
            System.out.println("arshvalue" + fName);

            textIn.setText("");
            //   et_symptom2=(EditText)findViewById(R.id.activity_info_edittext_symptom2);
            //   et_symptom3=(EditText)findViewById(R.id.activity_info_edittext_symptom3);
            et_details.setText("");
            et_additional_details.setText("");

            imageviewpropic.setBackgroundResource(0);


        }

        upload_image_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.report_or_symptoms = "symptom";
                pickImage();

            }
        });

        submit = (Button) findViewById(R.id.activity_registration_button_submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int selectedId = rg_gender.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                RadioButton radioButton = (RadioButton) findViewById(selectedId);

                String emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\\.+[a-z]+";
                if (et_name.getText().toString().equals("") && et_email_id.getText().toString().equals("") && textIn.getText().toString().equals("") && et_details.getText().toString().equals("") && et_age.getText().toString().equals("")) {

                    til_name.setErrorEnabled(true);
                    til_name.setError("Please enter the name");


                    til_age.setErrorEnabled(true);
                    til_age.setError("Please enter age");

                    til_symptoms.setErrorEnabled(true);
                    til_symptoms.setError("Please enter the Symptoms");


                    til_details.setErrorEnabled(true);
                    til_details.setError("Please enter the details");

                    til_email.setErrorEnabled(true);
                    til_email.setError("Please enter Email Id");

//                    et_name.setError("Please enter the name");
//                    et_age.setError("Please enter age");
//
//                    textIn.setError("Please enter the symptoms");
//                    et_details.setError("Please enter the details");
//                    et_email_id.setError("Please enter email id");

                } else if (et_name.getText().toString().equals("")) {
                    til_name.setErrorEnabled(true);
                    til_name.setError("Please enter the name");
//                    et_name.setError("Please enter the name");

                } else if (et_email_id.getText().toString().equals("")) {
                    til_email.setErrorEnabled(true);
                    til_email.setError("Please enter Email Id");
//                    et_email_id.requestFocus();
//                    et_email_id.setError("Please enter email id");
                } else if (et_age.getText().toString().equals("")) {
                    til_age.setErrorEnabled(true);
                    til_age.setError("Please enter age");
//                    et_age.requestFocus();
//                    et_age.setError("Please enter age");
                } else if (rg_gender.getCheckedRadioButtonId() == -1) {

                    Toast.makeText(getApplicationContext(), "Please select gender.", Toast.LENGTH_LONG).show();
                } else if (textIn.getText().toString().equals("")) {
                    textIn.requestFocus();
                    textIn.setError("Please enter the symptom");
                } else if (et_details.getText().toString().equals("")) {
                    til_details.setErrorEnabled(true);
                    til_details.setError("Please enter the details");
//                    et_details.requestFocus();
//                    et_details.setError("Please enter the details");
                } else if (!(Patterns.EMAIL_ADDRESS.matcher(et_email_id.getText().toString().trim()).matches())) {
                    til_email.setErrorEnabled(true);
                    til_email.setError("Please enter Valid Email ID");
//                    et_email_id.requestFocus();
//                    et_email_id.setError("Please enter valid email id");

                } else {


                    if (ConnectionDetection.isNetworkAvailable(Activity_save_data.this) == true) {
                        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);

                        // Set the progress dialog title and message
                        //  pd.setTitle("Saving Information");
                        pd.setMessage("Saving information..");

                        // Set the progress dialog background color
                        pd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFD4D9D0")));

                        pd.setIndeterminate(false);

                        // Finally, show the progress dialog
                        pd.show();
                        pd.setCancelable(false);
                        // progressDialog.show();

                        if (AppConstants.img == null) {
                            AppConstants.img = "";
                        }
                        System.out.println("ImageArsh" + AppConstants.img);
                               /* if(AppConstants.img.equals(null))
                                {
                                    AppConstants.img="";
                                }*/
                        appendd = "";
                        String first = textIn.getText().toString();
                        appendd += first;
                        int childCount = container.getChildCount();
                        for (int i = 0; i < childCount; i++) {
                            View thisChild = container.getChildAt(i);
                            //  reList.append(thisChild + "\n");

                            childTextView = (AutoCompleteTextView) thisChild.findViewById(R.id.textout);
                            String childTextViewValue = childTextView.getText().toString();
                            childTextView.setOnFocusChangeListener(Activity_save_data.this);
                            appendd += " , " + " " + childTextViewValue;


                        }


                        AppConstants.cname = et_name.getText().toString().replace("\n", " ");
                        AppConstants.email = et_email_id.getText().toString().replace("\n", " ");
                        AppConstants.symptom1 = appendd.replace("\n", " ");
                        AppConstants.additional_details = et_additional_details.getText().toString().replace("\n", " ");
                        AppConstants.age = et_age.getText().toString().replace("\n", " ");
                        AppConstants.gender = radioButton.getText().toString();
                        AppConstants.details = et_details.getText().toString().replace("\n", " ");
                        //  Savedata();
                        imageUpload(mSelectPath, mSymSelectPath);

                        //    finish();
                    }
                }
            }


        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

///select multiple images ///


    public void getAllPackages(final ListView timeLV1, final Dialog settingsDialog, final TextView mResultText3) {
        final ProgressDialog progressDialog = new ProgressDialog(Activity_save_data.this);
        progressDialog.setMessage("Please wait ....");
        progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(Activity_save_data.this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://designer321.com/kaushaljohn/ayurvedic/api/get_all_packages.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            Log.e("Response",response);
                            JSONObject jobj = new JSONObject(response);
                            String meg_code = jobj.getString("message_code");
                            if (meg_code.equals("1")) {

                                allPackagesArrayList.clear();
                                JSONArray jarray = jobj.getJSONArray("response");
                                for (int i = 0; i < jarray.length(); i++) {
                                    allPackages = new AllPackages();
                                    allPackages.setPackage_id(jarray.getJSONObject(i).getString("package_id"));
                                    allPackages.setPackage_name(jarray.getJSONObject(i).getString("package_name"));
                                    allPackages.setPackage_fee(jarray.getJSONObject(i).getString("package_fee"));
                                    allPackagesArrayList.add(allPackages);
                                }

                                timeLV1.setAdapter(new AllPackagesAdapter(Activity_save_data.this, allPackagesArrayList, settingsDialog, mResultText3));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
            }
        });
        RetryPolicy policy = new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                progressDialog.dismiss();
            }
        }, 20000);
        stringRequest.setShouldCache(false);
        queue.add(stringRequest);
    }

    public void selectPackage() {
        final Dialog settingsDialog = new Dialog(Activity_save_data.this);
        settingsDialog.getWindow().setTitle("Select Plan");
        settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.content_plans
                , null));
        settingsDialog.setCancelable(true);
        timeLV = (ListView) settingsDialog.findViewById(R.id.listView);
        AppConstants.plan = "";
        AppConstants.price = "";
        getAllPackages(timeLV, settingsDialog, mResultText3);

        settingsDialog.show();
    }

    private void pickImage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN // Permission was added in API Level 16
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
                    getString(R.string.mis_permission_rationale),
                    REQUEST_STORAGE_READ_ACCESS_PERMISSION);
        } else {

            int maxNum = 10;
            MultiImageSelector selector = MultiImageSelector.create(Activity_save_data.this);

            if (AppConstants.report_or_symptoms.equals("report")) {
                selector.showCamera(false);
                selector.count(maxNum);

                selector.multi();

                selector.origin(mSelectPath);
                selector.start(Activity_save_data.this, REQUEST_IMAGE);
            } else {
                selector.showCamera(true);
                selector.count(maxNum);

                selector.multi();

                selector.origin(mSymSelectPath);
                selector.start(Activity_save_data.this, REQUEST_IMAGE);
            }
        }
    }

    private void requestPermission(final String permission, String rationale, final int requestCode) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
            new AlertDialog.Builder(this, R.style.AlertDialogCustom)
                    .setTitle(R.string.mis_permission_dialog_title)
                    .setMessage(rationale)
                    .setPositiveButton(R.string.mis_permission_dialog_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(Activity_save_data.this, new String[]{permission}, requestCode);
                        }
                    })
                    .setNegativeButton(R.string.mis_permission_dialog_cancel, null)
                    .create().show();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_STORAGE_READ_ACCESS_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                pickImage();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK
                && (requestCode == ChooserType.REQUEST_PICK_PICTURE || requestCode == ChooserType.REQUEST_CAPTURE_PICTURE)) {
            if (imageChooserManager == null) {
                reinitializeImageChooser();
            }
            imageChooserManager.submit(requestCode, data);
        }
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == RESULT_OK) {


                if (AppConstants.report_or_symptoms.equals("report")) {

                    mSelectPath = data.getStringArrayListExtra(MultiImageSelector.EXTRA_RESULT);

                    StringBuilder sb = new StringBuilder();
                    for (String p : mSelectPath) {
                        sb.append(p);
                        sb.append("\n");
                    }
                    mResultText2.setText(sb.toString());
                } else {

                    mSymSelectPath = data.getStringArrayListExtra(MultiImageSelector.EXTRA_RESULT);

                    StringBuilder sb = new StringBuilder();
                    for (String p : mSymSelectPath) {
                        sb.append(p);
                        sb.append("\n");
                    }
                    mResultText.setText(sb.toString());
                }
                //mResultText2
            }
        }
    }


    /////////////////////////////////////////

    ///////////////custom listview for dynamic text add

    private void listAllAddView() {
        // reList.setText("");

        int childCount = container.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View thisChild = container.getChildAt(i);
            //  reList.append(thisChild + "\n");

            childTextView = (AutoCompleteTextView) thisChild.findViewById(R.id.textout);
            childTextView.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(childTextView, InputMethodManager.SHOW_IMPLICIT);
            String childTextViewValue = childTextView.getText().toString();
            childTextView.setHint("Symptoms");
            childTextView.setGravity(Gravity.CENTER);
            // reList.append("= " + childTextViewValue + "\n");
        }
    }


    ///////

    private void addEditView() {
        // TODO Auto-generated method stub
        final LinearLayout li = new LinearLayout(this);
        EditText et = new EditText(this);
        Button b = new Button(this);

        b.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                int pos = (Integer) v.getTag();
                li.removeViewAt(pos);

            }
        });

        b.setTag((li.getChildCount() + 1));
    }


    public void onError(final String reason) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                // pbar.setVisibility(View.GONE);
                Toast.makeText(getBaseContext(), reason,
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    private void reinitializeImageChooser() {
        imageChooserManager = new ImageChooserManager(this, chooserType,
                "myfolder", true);
        imageChooserManager.setImageChooserListener(Activity_save_data.this);
        imageChooserManager.reinitialize(filePath);
    }


    public void onImageChosen(final ChosenImage image) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {

                if (image != null) {

                    mResultText2.setText(new File(image.getFilePathOriginal()).toString());


                    try {
                        Bitmap bm = BitmapFactory.decodeFile(new File(image
                                .getFilePathOriginal()).toString());
                        int maxHeight = 200;
                        int maxWidth = 200;
                        float scale = Math.min(((float) maxHeight / bm.getWidth()), ((float) maxWidth / bm.getHeight()));

                        Matrix matrix = new Matrix();
                        matrix.postScale(scale, scale);

                        Bitmap scaled_bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        scaled_bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object
                        byte[] byteArrayImage = baos.toByteArray();

                        img_str = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
                        AppConstants.img = img_str;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            }
        });

    }
    /////////////save data web service function////////////////////////////////

    //////////////////////////////////////////

/////save with multiple images
    ////////////////image upload ////////////////////////////////////


    public void imageUpload(ArrayList<String> mSelectPath, ArrayList<String> mSymSelectPath) {

        SimpleMultiPartRequest smr = new SimpleMultiPartRequest(Request.Method.POST, SAVE_DATA_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        Log.d("Response", response);
                        //    Toast.makeText(getApplicationContext(),"ress"+response,Toast.LENGTH_LONG).show();
                        try {
                            JSONObject j = new JSONObject(response);
                            System.out.println("resArsh" + response.toString() + response.toString());
                            int msg_code = j.getInt("message_code");
                            res = j.getString("message");

                            if (msg_code == 1) {
                                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(Activity_save_data.this, R.style.AlertDialogCustom);
                                builder.setMessage("Thank you. All our online Ayurvedacharya  have been notified and will get back to you soon.")
                                        .setCancelable(false)
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {


                                                et_name.setText("");
                                                et_email_id.setText("");
                                                et_age.setText("");
                                                et_details.setText("");
                                                et_additional_details.setText("");
                                                textIn.setText("");

                                                rg_gender.clearCheck();
                                                mResultText.setText("");
                                                mResultText3.setText("");
                                                mResultText2.setText("");

                                                AppConstants.cname = "";
                                                AppConstants.email = "";
                                                AppConstants.symptom1 = "";
                                                AppConstants.symptom2 = "";
                                                AppConstants.symptom3 = "";
                                                AppConstants.details = "";
                                                AppConstants.additional_details = "";
                                                AppConstants.img = "";
                                                AppConstants.plan = "";
                                                AppConstants.price = "";
                                                AppConstants.plan_id = "";
                                                AppConstants.gender = "";
                                                AppConstants.age = "";

                                            }
                                        });
                                android.support.v7.app.AlertDialog alert = builder.create();
                                alert.show();
                            } else {
                                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(Activity_save_data.this, R.style.AlertDialogCustom);
                                builder.setMessage(res)
                                        .setCancelable(false)
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {


                                            }
                                        });
                                android.support.v7.app.AlertDialog alert = builder.create();
                                alert.show();
                            }


                        } catch (JSONException e) {
                            System.out.println("jsonexeption1111" + e.toString());
                        }

//                        JSONObject j = null;
//                        try {
//                            j = new JSONObject(response);
//                            System.out.println("resArsh" + response.toString() + response.toString());
//                            res = j.getString("message");
//                            payment_id = j.getString("payment_id");
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//                        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(Activity_save_data.this);
//                        SharedPreferences.Editor editor = settings.edit();
//                        editor.putString("payment_id", payment_id);
//                        editor.commit();

//                        Intent i = new Intent(Activity_save_data.this, Plans.class);
//                        startActivity(i);
//                        finish();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Please Check your internet connection.", Toast.LENGTH_LONG).show();
            }
        });

        try {
            for (int i = 0; i < mSymSelectPath.size(); i++) {
                smr.addFile("img[" + i + "]", String.valueOf(mSymSelectPath.get(i)));
                System.out.println("Arsh Op" + "img[" + i + "]" + String.valueOf(mSelectPath));
            }
        } catch (Exception e) {

        }

        try {
            for (int i = 0; i < mSelectPath.size(); i++) {
                smr.addFile("medical_reports[" + i + "]", String.valueOf(mSelectPath.get(i)));
                System.out.println("Arsh Op" + "medical_reports[" + i + "]" + String.valueOf(mSelectPath));
            }
        } catch (Exception e) {

        }
        smr.addStringParam(KEY_CUSTOMER_ID, customer_id);
        smr.addStringParam(KEY_NAME, AppConstants.cname);
        smr.addStringParam(KEY_EMAIL, AppConstants.email);
        smr.addStringParam(KEY_GENDER, AppConstants.gender);
        smr.addStringParam(KEY_AGE, AppConstants.age);
        smr.addStringParam(KEY_SYMPTOM1, AppConstants.symptom1);
        smr.addStringParam(KEY_SYMPTOM_PIC, "");
        smr.addStringParam(KEY_DETAILS, AppConstants.details);
        smr.addStringParam(KEY_DEVICE_TYPE, "Android");
        smr.addStringParam(KEY_ADDITIONAL_DETAILS, AppConstants.additional_details);
        smr.addStringParam(KEY_PICTURE_FILE, AppConstants.img);
        smr.addStringParam(KEY_MEDICAL_DOC, "med doc");
        smr.addStringParam(KEY_VIDEO_FILE, "video");
        smr.addStringParam(KEY_PACKAGE_NAME, AppConstants.plan);
        smr.addStringParam(KEY_AMOUNT_PAID, AppConstants.price);


        Log.e("Param", smr.toString());
        AppController.getInstance().addToRequestQueue(smr);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            SharedPreferences.Editor editor = settings.edit();

            isLogin = "";
            editor.putString("isLogin", isLogin);
            editor.commit();
//            Intent intent = new Intent(Activity_save_data.this, Activity_login.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(intent);
//            finishAffinity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Activity_save_data Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    @Override
    public void onFocusChange(View v, boolean b) {
        switch (v.getId()) {
            case R.id.activity_registration_edittext_full_name:
                if (b) {
                    til_name.setErrorEnabled(false);
                    til_name.setError(null);

//                    nameicon.setImageResource(R.drawable.username_blue);
                } else {
//                    nameicon.setImageResource(R.drawable.username_icon);
                }
                break;
            case R.id.activity_login_edittext_email_id:
                if (b) {

                    til_email.setError(null);
//                    emailicon.setImageResource(R.drawable.mail_icon_blue);
                } else {
//                    emailicon.setImageResource(R.drawable.mail_icon);
                }
                break;
            case R.id.activity_login_edittext_age:
                if (b) {
                    til_age.setError(null);
//                    ageicon.setImageResource(R.drawable.age_icon_blue);
                } else {
//                    ageicon.setImageResource(R.drawable.age_icon);
                }
                break;
            case R.id.textin:
                if (b) {
                    til_symptoms.setError(null);

//                    symptomicon.setImageResource(R.drawable.symptoms_icon_blue);
                } else {
//                    symptomicon.setImageResource(R.drawable.symptoms_icon);
                }
                break;
            case R.id.activity_info_edittext_details:
                if (b) {
                    til_details.setError(null);
//                    detailsicon.setImageResource(R.drawable.detials_icon_blue);
                } else {
//                    detailsicon.setImageResource(R.drawable.detials_icon);
                }

                break;
            case R.id.activity_info_edittext_additional_details:
                if (b) {
                    til_add_details.setError(null);
//                    adddetailsicon.setImageResource(R.drawable.additional_details_blue);
                } else {
//                    adddetailsicon.setImageResource(R.drawable.additional_details_icon);
                }
                break;
            case R.id.textout:
                if (b) {

//                    addsymptomsicon.setImageResource(R.drawable.symptoms_icon_blue);
                } else {
//                    addsymptomsicon.setImageResource(R.drawable.symptoms_icon);
                }
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (editable == et_name.getEditableText()) {
            til_name.setErrorEnabled(false);
            til_name.setError(null);

//            nameicon.setImageResource(R.drawable.username_blue);
        } else if (editable == et_email_id.getEditableText()) {

            til_email.setError(null);
//            emailicon.setImageResource(R.drawable.mail_icon_blue);
        } else if (editable == et_age.getEditableText()) {
            til_age.setError(null);
//            ageicon.setImageResource(R.drawable.age_icon_blue);
        } else if (editable == textIn.getEditableText()) {
            til_symptoms.setError(null);

//            symptomicon.setImageResource(R.drawable.symptoms_icon_blue);
        } else if (editable == et_details.getEditableText()) {
            til_details.setError(null);
//            detailsicon.setImageResource(R.drawable.detials_icon_blue);
        } else if (editable == et_additional_details.getEditableText()) {
            til_add_details.setError(null);
//            adddetailsicon.setImageResource(R.drawable.additional_details_blue);
        } else if (editable == childTextView.getEditableText()) {

//            addsymptomsicon.setImageResource(R.drawable.symptoms_icon_blue);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.abhyangaIV:
                break;
            case R.id.vataDoshaIV:
                break;
            case R.id.pitta_doshaIV:
                break;
            case R.id.pulse_diagnosisIV:
                break;
            case R.id.rasayanaIV:
                break;
        }
    }
}
