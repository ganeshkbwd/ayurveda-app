package hnweb.com.ayurvedaapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;

import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends Activity implements ImageChooserListener {

    AutoCompleteTextView textIn;
    Button buttonAdd;
    LinearLayout container;
    String appendd = "";
    AutoCompleteTextView childTextView;
    private static final String[] NUMBER = new String[] {
            "One", "Two", "Three", "Four", "Five",
            "Six", "Seven", "Eight", "Nine", "Ten"
    };
    ArrayAdapter<String> adapter;

    ProgressDialog pdialog;
    String isLogin;
    Button submit,upload_image_button,logout,upload_docs;
    EditText et_name,et_email_id,et_symptom1, et_details,et_additional_details,et_age;
    RadioGroup rg_gender;
    String name,email,symptom1,symptom2,symptom3,details,image,res,payment_id;
    ImageView imageviewpropic;
    private ImageChooserManager imageChooserManager;
    private String filePath;
    private int chooserType;
    String img_str,customer_id,full_name,email_address;
    ProgressDialog progressDialog;
    AutoCompleteTextView textOut;

    SharedPreferences settings;
    /////////////////////// web service to save data ////////////////////////
    ///buy views

    private static final String SAVE_DATA_URL = "http://designer321.com/kaushaljohn/ayurvedic/api/save_customer_details.php";



    public static final String KEY_CUSTOMER_ID = "customer_id";

   /// public static final String KEY_FULLNAME = "name";
   public static final String KEY_NAME ="name";
    public static final String KEY_EMAIL ="email";
    public static final String KEY_GENDER ="gender";
    public static final String KEY_AGE ="age";
    public static final String KEY_SYMPTOM1 ="symptom1";
    public static final String KEY_SYMPTOM_PIC ="picture_of_symptom";
    public static final String KEY_DETAILS ="details";
    public static final String KEY_DEVICE_TYPE ="device_type";
    public static final String KEY_ADDITIONAL_DETAILS ="additional_details";
    public static final String KEY_PICTURE_FILE ="picture_file";

    public static final String KEY_MEDICAL_DOC ="medical_document_file";
    public static final String KEY_VIDEO_FILE ="video_file";

    public static final String KEY_PACKAGE_NAME ="package_name";
    public static final String KEY_AMOUNT_PAID ="amount_paid";
    public static final String KEY_PAYMENT_TXN_ID ="payment_txn_id";








    /////////////////////////////////////////////////////////////////////////

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

//http://android-er.blogspot.in/2015/12/add-and-remove-view-dynamically-with.html  for dynamic edit text



    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pdialog=new ProgressDialog(this);

        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, NUMBER);

        textIn = (AutoCompleteTextView)findViewById(R.id.textin);
        textIn.setAdapter(adapter);

        buttonAdd = (Button)findViewById(R.id.add);
        container = (LinearLayout) findViewById(R.id.container);

        settings = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);

        rg_gender=  (RadioGroup) findViewById(R.id.radioGroup1);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
      //  setSupportActionBar(toolbar);
        progressDialog= new ProgressDialog(MainActivity.this);

        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        customer_id = settings.getString("customer_id", "");
        full_name = settings.getString("full_name", "");
        email_address=settings.getString("email_address","");
LinearLayout l1=(LinearLayout)findViewById(R.id.ll1);

        buttonAdd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflater =
                        (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View addView = layoutInflater.inflate(R.layout.layout_row, null);
                 textOut = (AutoCompleteTextView)addView.findViewById(R.id.textout);
                textOut.setAdapter(adapter);
                // textOut.setText(textIn.getText().toString());
                Button buttonRemove = (Button)addView.findViewById(R.id.remove);

                final View.OnClickListener thisListener = new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        //    info.append("thisListener called:\t" + this + "\n");
                        //    info.append("Remove addView: " + addView + "\n\n");
                        ((LinearLayout)addView.getParent()).removeView(addView);

                        listAllAddView();
                    }
                };

                buttonRemove.setOnClickListener(thisListener);
                container.addView(addView);

             /*   info.append(
                        "thisListener:\t" + thisListener + "\n"
                                + "addView:\t" + addView + "\n\n"
                );*/

                listAllAddView();
            }
        });


      /*  RelativeLayout layout = (RelativeLayout) findViewById(R.id.rl1);
        for(int i=0;i<10;i++)
        {
            ImageView image = new ImageView(this);
            image.setBackgroundResource(R.drawable.ic_launcher);
            image.setLayoutParams(new android.view.ViewGroup.LayoutParams(80,60));
            image.setMaxHeight(20);
            image.setMaxWidth(20);

            // Adds the view to the layout
            layout.addView(image);
        }*/

        logout=(Button)findViewById(R.id.logout_button);


//        logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//              //  settings = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
//
//
//                AppConstants.cname="";
//                AppConstants.email="";
//                AppConstants.symptom1="";
//                AppConstants.symptom2="";
//                AppConstants.symptom3="";
//                AppConstants.details="";
//                AppConstants.additional_details="";
//                AppConstants.img="";
//                AppConstants.plan="";
//                AppConstants.price="";
//                AppConstants.plan_id="";
//                AppConstants.gender="";
//                AppConstants.age="";
//                SharedPreferences.Editor editor = settings.edit();
//
//                isLogin="";
//                editor.putString("isLogin",isLogin);
//                editor.commit();
//
////                Intent intent = new Intent(MainActivity.this, Activity_login.class);
////                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
////                startActivity(intent);
////                finishAffinity();
//            }
//        });
///Toast.makeText(getApplicationContext(),"customer id"+customer_id+"fullname"+full_name+"email"+email_address,Toast.LENGTH_LONG).show();
     /*   editor.putString("customer_id", customer_id);
        editor.putString("full_name", full_name);
        editor.putString("email_address", email_address);*/


        et_age=(EditText)findViewById(R.id.activity_login_edittext_age);
  //      setSupportActionBar(toolbar);
        et_name=(EditText)findViewById(R.id.activity_registration_edittext_full_name);
        et_name.setText(full_name);

        et_email_id=(EditText)findViewById(R.id.activity_login_edittext_email_id);
        et_email_id.setText(email_address);

        et_symptom1=(EditText)findViewById(R.id.activity_login_edittext_password);
     //   et_symptom2=(EditText)findViewById(R.id.activity_info_edittext_symptom2);
     //   et_symptom3=(EditText)findViewById(R.id.activity_info_edittext_symptom3);
        et_details=(EditText)findViewById(R.id.activity_info_edittext_details);
        et_additional_details=(EditText)findViewById(R.id.activity_info_edittext_additional_details);

        imageviewpropic=(ImageView)findViewById(R.id.activity_info_imageview_upload_image);
        submit=(Button)findViewById(R.id.activity_registration_button_submit);
        upload_docs=(Button)findViewById(R.id.activity_info_button_upload_image1);

        upload_docs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getApplicationContext(),"Under Developement",Toast.LENGTH_LONG).show();

               // addEditView();
            }
        });

        upload_image_button=(Button)findViewById(R.id.activity_info_button_upload_image);
        Intent intent = getIntent();

        String fName = intent.getStringExtra("pay_success");
        // String value = getIntent().getExtras().getString("pay_success");
        if(fName!= null)
        {
            //System.out.println(""+fName);
            System.out.println("arshvalue"+fName);

            textIn.setText("");
            //   et_symptom2=(EditText)findViewById(R.id.activity_info_edittext_symptom2);
            //   et_symptom3=(EditText)findViewById(R.id.activity_info_edittext_symptom3);
            et_details.setText("");
            et_additional_details.setText("");

            imageviewpropic.setBackgroundResource(0);


        }

        upload_image_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              //  Toast.makeText(MainActivity.this,"Clicked",Toast.LENGTH_LONG).show();
                selectImage(imageviewpropic);
            }
        });

        submit=(Button)findViewById(R.id.activity_registration_button_submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int selectedId = rg_gender.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                RadioButton radioButton = (RadioButton) findViewById(selectedId);

             /*   Toast.makeText(MainActivity.this,
                        radioButton.getText(), Toast.LENGTH_SHORT).show();
*/


              /*  AppConstants.cname=null;
                AppConstants.email=null;
                AppConstants.symptom1=null;
                AppConstants.symptom2=null;
                AppConstants.symptom3=null;
                AppConstants.details=null;
                AppConstants.additional_details=null;
                AppConstants.img=null;
                AppConstants.plan=null;
                AppConstants.price=null;
                AppConstants.plan_id=null;*/

                String emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\\.+[a-z]+";
                if(et_name.getText().toString().equals("")&&et_email_id.getText().toString().equals("")&&textIn.getText().toString().equals("")&&et_details.getText().toString().equals("")&&et_age.getText().toString().equals(""))
                {
                    et_name.setError("Please enter the name");
                    et_email_id.setError("Please enter the email id");
                    textIn.setError("Please enter the Symptoms");
                    et_details.setError("Please enter the details");
                    et_age.setError("Please enter age");
                }
                else if (et_name.getText().toString().equals(""))
                {
                    et_name.setError("Please enter the name");

                }
                else if(et_email_id.getText().toString().equals(""))
                {
                    et_email_id.setError("Please enter Email Id");
                }
               else if (textIn.getText().toString().equals(""))
                {
                    textIn.setError("Please enter the Symptom");
                }
                else if (et_details.getText().toString().equals(""))
                {
                    et_details.setError("Please enter the Details");
                }
                else if (et_age.getText().toString().equals(""))
                {
                    et_age.setError("Please enter the Age");
                }
                else if((et_email_id.getText().toString().matches(emailPattern))==false)
                {
                    et_email_id.setError("Please enter Valid Email ID");

                }


               else if (rg_gender.getCheckedRadioButtonId() == -1)
                {
                  Toast.makeText(getApplicationContext(),"Please Select Gender",Toast.LENGTH_LONG).show();
                }
             /*else if(img_str.toString().equals(""))
                {
                    Toast.makeText(getApplicationContext(),"Please upload image",Toast.LENGTH_SHORT).show();
                }*/
                else
                {




                        if (ConnectionDetection.isNetworkAvailable(MainActivity.this) == true) {
                            if (!progressDialog.isShowing()) {
                                progressDialog.setMessage("Registering User ,Please wait ...");
                                // progressDialog.show();

                                if(AppConstants.img == null)
                                {
                                    AppConstants.img="";
                                }
                                System.out.println("ImageArsh"+AppConstants.img);
                               /* if(AppConstants.img.equals(null))
                                {
                                    AppConstants.img="";
                                }*/
                                appendd="";
                                String first= textIn.getText().toString();
                                appendd+=first;
                                int childCount = container.getChildCount();
                                for(int i=0; i<childCount; i++){
                                    View thisChild = container.getChildAt(i);
                                    //  reList.append(thisChild + "\n");

                                    childTextView = (AutoCompleteTextView) thisChild.findViewById(R.id.textout);
                                    String childTextViewValue = childTextView.getText().toString();
                                    //  Toast.makeText(getApplicationContext(),""+childTextViewValue,Toast.LENGTH_LONG).show();
                                    //   String s= childTextViewValue.append("= " + childTextViewValue + "\n");





                                    appendd += " , "+" "+childTextViewValue;



                                }


                                AppConstants.cname=et_name.getText().toString().replace("\n"," ");
                                AppConstants.email=et_email_id.getText().toString().replace("\n"," ");
                                AppConstants.symptom1=appendd.replace("\n"," ");
                                AppConstants.additional_details=et_additional_details.getText().toString().replace("\n"," ");
                                AppConstants.age=et_age.getText().toString().replace("\n"," ");
                                AppConstants.gender=radioButton.getText().toString();
                           /*     AppConstants.symptom2=et_symptom2.getText().toString();
                                AppConstants.symptom3=et_symptom3.getText().toString();
                             */   AppConstants.details=et_details.getText().toString().replace("\n"," ");
                                Savedata();


                            //    finish();
                            }
                        }
                }

                }





        });
    }



    ///////////////custom listview for dynamic text add

    private void listAllAddView(){
        // reList.setText("");

        int childCount = container.getChildCount();
        for(int i=0; i<childCount; i++){
            View thisChild = container.getChildAt(i);
            //  reList.append(thisChild + "\n");

            childTextView = (AutoCompleteTextView) thisChild.findViewById(R.id.textout);
            String childTextViewValue = childTextView.getText().toString();
            // reList.append("= " + childTextViewValue + "\n");
        }
    }


    ///////

    private void addEditView() {
        // TODO Auto-generated method stub
        final LinearLayout li=new LinearLayout(this);
        EditText et=new EditText(this);
        Button b=new Button(this);

        b.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                int pos=(Integer) v.getTag();
                li.removeViewAt(pos);

            }
        });

        b.setTag((li.getChildCount()+1));
    }


    ///////////////image choosing code
    public void selectImage(View view) {
        final CharSequence[] items = {"Take Photo", "Choose From Gallery",
                "Cancel"};
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Add Photo");
        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    takePicture();
                } else if (items[item].equals("Choose From Gallery")) {
                    chooseImage();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void chooseImage() {
        chooserType = ChooserType.REQUEST_PICK_PICTURE;
        imageChooserManager = new ImageChooserManager(this,
                ChooserType.REQUEST_PICK_PICTURE, "myfolder", true);
        imageChooserManager.setImageChooserListener(MainActivity.this);
        try {
            filePath = imageChooserManager.choose();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void takePicture() {
        chooserType = ChooserType.REQUEST_CAPTURE_PICTURE;
        imageChooserManager = new ImageChooserManager(this,
                ChooserType.REQUEST_CAPTURE_PICTURE, "myfolder", true);
        imageChooserManager.setImageChooserListener(MainActivity.this);
        try {
            filePath = imageChooserManager.choose();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK
                && (requestCode == ChooserType.REQUEST_PICK_PICTURE || requestCode == ChooserType.REQUEST_CAPTURE_PICTURE)) {
            if (imageChooserManager == null) {
                reinitializeImageChooser();
            }
            imageChooserManager.submit(requestCode, data);
        }
    }


    public void onError(final String reason) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                // pbar.setVisibility(View.GONE);
                Toast.makeText(getBaseContext(), reason,
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    private void reinitializeImageChooser() {
        imageChooserManager = new ImageChooserManager(this, chooserType,
                "myfolder", true);
        imageChooserManager.setImageChooserListener(MainActivity.this);
        imageChooserManager.reinitialize(filePath);
    }


    public void onImageChosen(final ChosenImage image) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {

                if (image != null) {


                    imageviewpropic.setImageURI(Uri.parse("file://" + new File(image
                            .getFilePathOriginal()).toString()));

//                    BitmapDrawable drawable = (BitmapDrawable) imageviewpropic.getDrawable();
//                    Bitmap bitmap = drawable.getBitmap();
                    imageviewpropic.buildDrawingCache();
                  /*  Bitmap bitmap = imageviewpropic.getDrawingCache();

                    ByteArrayOutputStream stream=new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream);
                    byte[] image=stream.toByteArray();
                    System.out.println("byte array:"+image);

                    String img_str = Base64.encodeToString(image, 0);
                    System.out.println("base64string "+img_str);

///////////////////////////





////////////////
*/


                    /////////////////

               /*     Bitmap myBitmap = BitmapFactory.decodeFile(new File(image
                            .getFilePathOriginal()).toString());
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    try {
                        GZIPOutputStream gzipOstream = null;
                        try {
                            gzipOstream = new GZIPOutputStream(stream);
                            myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, gzipOstream);
                            gzipOstream.flush();
                        } finally {
                            gzipOstream.close();
                            stream.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        stream = null;
                    }
                    if(stream != null) {
                        byte[] byteArry=stream.toByteArray();
                        String encodedImage = Base64.encodeToString(byteArry, Base64.NO_WRAP);
                        img_str = encodedImage;
System.out.println("myimage"+img_str);
                        // do something with encodedImage
                    }*/

                   /////////////////
                  /*  Bitmap bm = BitmapFactory.decodeFile( new File(image
                            .getFilePathOriginal()).toString());
                    ByteArrayOutputStream baos = new ByteArrayOutputStream(); bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    bm.compress(Bitmap.CompressFormat.PNG, 90, baos);
                    byte[] image=baos.toByteArray();
                    //   System.out.println("byte array:"+image);

                    img_str = Base64.encodeToString(image, Base64.DEFAULT);
                    System.out.println("base64string "+img_str);
                    AppConstants.img=img_str;
*/
                    //bm is the bitmap object byte[] b = baos.toByteArray();
                    // System.out.println("bitmapcode"+bitmap.toString());







                    try {
                        Bitmap bm = BitmapFactory.decodeFile( new File(image
                                .getFilePathOriginal()).toString());
                        int maxHeight = 200;
                        int maxWidth = 200;
                        float scale = Math.min(((float)maxHeight / bm.getWidth()), ((float)maxWidth / bm.getHeight()));

                        Matrix matrix = new Matrix();
                        matrix.postScale(scale, scale);

                        Bitmap scaled_bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        scaled_bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object
                        byte[] byteArrayImage = baos.toByteArray();

                         img_str = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
                        AppConstants.img=img_str;
                      /*  if(encodedImage != null)
                        {
                            return encodedImage;
                        }*/
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }




            }
        });

    }
    /////////////save data web service function////////////////////////////////
    private void Savedata()
    {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SAVE_DATA_URL,
                new Response.Listener<String>() {

                    public void onResponse(String response) {
                        System.out.println("res= " + response);
                        //    System.out.println("reg"+GET_PLANS__URL.toString()+response.toString());
                        pdialog.dismiss();
                        try {
                            JSONObject j = new JSONObject(response);
                            System.out.println("resArsh"+ response.toString()+response.toString());
                            res = j.getString("message");
                            payment_id=j.getString("payment_id");

                            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                            SharedPreferences.Editor editor = settings.edit();
                            editor.putString("payment_id", payment_id);
                            editor.commit();

                            Intent i =new Intent(MainActivity.this, Plans.class);
                            startActivity(i);
                          //  customer_id = settings.getString("customer_id", "");

                            Toast.makeText(getApplicationContext(),"Data"+res+"payment id"+payment_id,Toast.LENGTH_LONG).show();


                        }
                        catch(JSONException e)
                        {
                            System.out.println("jsonexeption1111"+e.toString());
                        }
                        ////////////////////////////////////////////////
//                        if(progressDialog.isShowing()) {
//                            progressDialog.hide();
//                        }
                        //  Toast.makeText(Getview_signup_activity.this,response,Toast.LENGTH_LONG).show();


                        //{"message_code":1,"message":"Registered successfully.","uid":"48","user":{"name":"Arsh","email":"aarsh@gmail.co","created_at":"2016-06-13 23:59:21"}}




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_LONG).show();
                        System.out.println("jsonexeption"+error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                try {
                 //   SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                 //   SharedPreferences.Editor editor = settings.edit();
                 //   selected_plan = settings.getString("selected_plan", "");
                 //   selected_price = settings.getString("selected_price", "");
                //    selected_plan_id=settings.getString("selected_plan_id","");
                //    selected_views_count=settings.getString("selected_views_count","");
                    //selected_views_count
                //    final String userid ="2";
                 //   final String video_thumb_url=selected_video_url;
                    final String video_type="Instagram";
                    final String device="Android";
                  //  final String transaction_id="asdfasdfasdfsadf";
                  //  final String plan_id=selected_plan_id;
                  //  final String plan_name=selected_plan;
                  //  final String views_count=selected_views_count;
                  //  final String fee=selected_price;
                  //  final String media_id=selected_video_id;
                 //   System.out.println("Arplanpostbefore"+price+plan+plan_id+views_count);
                 //   System.out.println("DataArsh"+"selected_plan_id"+selected_plan_id+"selected_plan"+selected_plan+"views_count"+views_count+"fee"+fee+"mediaid"+media_id);


                    params.put(KEY_CUSTOMER_ID,customer_id);
                    params.put(KEY_NAME, AppConstants.cname);
                    params.put(KEY_EMAIL ,AppConstants.email);
                    params.put(KEY_GENDER ,AppConstants.gender);
                    params.put(KEY_AGE ,AppConstants.age);
                    params.put(KEY_SYMPTOM1 , AppConstants.symptom1);
                    params.put(KEY_SYMPTOM_PIC ,"");
                    params.put(KEY_DETAILS , AppConstants.details);
                    params.put(KEY_DEVICE_TYPE , "Android");
                    params.put(KEY_ADDITIONAL_DETAILS ,AppConstants.additional_details);
                    params.put(KEY_PICTURE_FILE,AppConstants.img);


                   /* params.put(KEY_PACKAGE_NAME , AppConstants.plan);
                    params.put(KEY_AMOUNT_PAID , AppConstants.price);
                    params.put(KEY_PAYMENT_TXN_ID , AppConstants.txn_id);*/



                    params.put(KEY_MEDICAL_DOC ,"med doc");
                    params.put(KEY_VIDEO_FILE ,"video");


                    System.out.println("cname"+AppConstants.cname+"email"+AppConstants.email+"gender"+AppConstants.gender);






                  /*  public static final String KEY_NAME ="name";
                    public static final String KEY_EMAIL ="email";
                    public static final String KEY_GENDER ="age";
                    public static final String KEY_SYMPTOM_PIC ="picture_of_symptom";*/


                    //    params.put(KEY_SYMPTOM2 , AppConstants.symptom2);
                    //   params.put(KEY_SYMPTOM3  , AppConstants.symptom3);




/*
                    additional_details
                            picture_file
                    medical_document_file
                            video_file*/
                } catch (Exception e) {
                    System.out.println("arsherror" + e.toString());
                }


                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    /////////////////////////////////////



///////////////////

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            SharedPreferences.Editor editor = settings.edit();

            isLogin="";
            editor.putString("isLogin",isLogin);
            editor.commit();

//            Intent intent = new Intent(MainActivity.this, Activity_login.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(intent);
//            finishAffinity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
