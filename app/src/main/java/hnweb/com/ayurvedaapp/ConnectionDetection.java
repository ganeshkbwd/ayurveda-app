package hnweb.com.ayurvedaapp;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by HNWeb-01 on 5/21/2016.
 */
public class ConnectionDetection {
    public static boolean isNetworkAvailable(final Context context) {

        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
}
