package hnweb.com.ayurvedaapp;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by neha on 1/3/2017.
 */
public class AllPackagesAdapter extends BaseAdapter {
    Context context;
    ArrayList<AllPackages> allPackagesArrayList;
    Dialog settingsDialog;
    TextView mResultText3;

    public AllPackagesAdapter(Activity activity, ArrayList<AllPackages> allPackagesArrayList, Dialog settingsDialog, TextView mResultText3) {
        this.context = activity;
        this.allPackagesArrayList = allPackagesArrayList;
        this.settingsDialog = settingsDialog;
        this.mResultText3 = mResultText3;
    }

    @Override
    public int getCount() {
        return allPackagesArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return allPackagesArrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return allPackagesArrayList.lastIndexOf(i);
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {
        ViewHolder holder = null;
        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.views_price_list_layout, null);
            holder = new ViewHolder();

            holder.package_nameTV = (TextView) convertView.findViewById(R.id.driver_show_pickup_list_customer_name);
            holder.package_feeTV = (TextView) convertView.findViewById(R.id.driver_show_pickup_list_total_price);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.package_nameTV.setText(allPackagesArrayList.get(i).getPackage_name());
        holder.package_feeTV.setText(" $ " + allPackagesArrayList.get(i).getPackage_fee());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppConstants.plan = allPackagesArrayList.get(i).getPackage_name();
                AppConstants.price = " $ " + allPackagesArrayList.get(i).getPackage_fee();
                mResultText3.setText(AppConstants.plan + "   " + AppConstants.price);
                settingsDialog.dismiss();

                Log.e("PRICE", AppConstants.plan + "---" + AppConstants.price);
            }
        });

        return convertView;
    }

    private class ViewHolder {
        TextView package_nameTV;
        TextView package_feeTV;

    }
}
