package hnweb.com.ayurvedaapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalProfileSharingActivity;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Plans extends Activity {

    String plan, price, plan_id;
    BigDecimal money;
    SharedPreferences settings;

    String img_str, customer_id, full_name, email_address;
    ///paypal
    private static final String TAG = "paymentExample";
    /**
     * - Set to PayPalConfiguration.ENVIRONMENT_PRODUCTION to move real money.
     * <p>
     * - Set to PayPalConfiguration.ENVIRONMENT_SANDBOX to use your test credentials
     * from https://developer.paypal.com
     * <p>
     * - Set to PayPalConfiguration.ENVIRONMENT_NO_NETWORK to kick the tires
     * without communicating to PayPal's servers.
     */


    ///buy views

    private static final String BUY_VIEWS_URL = "http://designer321.com/kaushaljohn/ayurvedic/api/purchase_package.php";


    public static final String KEY_PAYMENT_ID = "payment_id";
    public static final String KEY_PACKAGE_NAME = "package_name";
    public static final String KEY_AMOUNT_PAID = "amount_paid";
    public static final String KEY_PAYMENT_TXN_ID = "payment_txn_id";
    /*
    public static final String KEY_CUSTOMER_ID = "customer_id";

    public static final String KEY_FULLNAME = "fullname";
    public static final String KEY_SYMPTOM1 ="symptom1";
    public static final String KEY_DETAILS ="details";


    public static final String KEY_PAYMENT_TXN_ID ="payment_txn_id";
    public static final String KEY_DEVICE_TYPE ="device_type";
    public static final String KEY_ADDITIONAL_DETAILS ="additional_details";
    public static final String KEY_PICTURE_FILE ="picture_file";
    public static final String KEY_MEDICAL_DOC ="medical_document_file";
    public static final String KEY_VIDEO_FILE ="video_file";
    public static final String KEY_NAME ="name";
    public static final String KEY_EMAIL ="email";
    public static final String KEY_GENDER ="gender";
    public static final String KEY_AGE ="age";
    public static final String KEY_SYMPTOM_PIC ="picture_of_symptom";

*/
 /*   name
            email
    gender
            age
    picture_of_symptom
*/

   /* public static final String KEY_EMAIL_ID="email_address";






    public static final String KEY_SYMPTOM2 ="symptom2";
    public static final String KEY_SYMPTOM3 ="symptom3";*/
    //  public static final String KEY_PICTURE1="picture1";


//////////////////////
/*
additional_details
        picture_file
    medical_document_file
            video_file
*/

    private static final String GET_PLANS__URL = "http://designer321.com/kaushaljohn/ayurvedic/api/add_customer_details.php";


    ///////////////////////////////////////
    String video_type, video_id, video_url, title_of_video, exp_view, total_views, who_uploaded, uploaded_date, video_thumb_url, media_id, total_purchased_views;


    //
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_PRODUCTION;

    // note that these credentials will differ between live & sandbox environments.
    //   test//private static final String CONFIG_CLIENT_ID = "AYyqCD4cl-7lmuSwL5ruNpLLB8590R9e00ez7o0gsoe-tpmVvj2o-RGOVIxkysz04FjMzIRgwXzbCHjz";

    //origional
    private static final String CONFIG_CLIENT_ID = "AXtDPTcIyVTbAb_Pp26sohP7vQRrsQwl1aUra8HspY8lG9_nbcGSXWZfu4YA6i924KLwr46hmRTwwAyR";

    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static final int REQUEST_CODE_PROFILE_SHARING = 3;

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
            // The following are only used in PayPalFuturePaymentActivity.
            .merchantName("Example Merchant")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));

    //

    ListView listOne;
    Button button_allorders, button_pending, button_pickup, button_completed, button_skip;
    //    ArrayList<ListItemModel1> mylist;
    //  String flag;
    String jsonResponse, email, mobile, address, usertype, data, flag, message, uname, pass, res, men;
    String order_id, pickup_date, status, sum, driver_id;
    String user_id, name, coins, p_photo, selected_video_url, selected_video_id, selected_plan, selected_price, selected_plan_id, selected_views_count;
    int tabflag;
    // String contstant_filter_tabs;
    String gv_plan_id, plan_name, views_count, fee, payment_id;
    //SharedPreference sharedPreference;
    ArrayAdapter<Views_price_list_item> adapter;
    ArrayList<Views_price_list_item> mylist;
    private Toolbar toolbar;
    //  ProgressBar progress;
    TextView username_text, coins_text;
    ProgressDialog pdialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plans);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // setSupportActionBar(toolbar);

        pdialog = new ProgressDialog(this);

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        customer_id = settings.getString("customer_id", "");
        full_name = settings.getString("full_name", "");
        email_address = settings.getString("email_address", "");
        payment_id = settings.getString("payment_id", "");

        listOne = (ListView) findViewById(R.id.listView);
        showpickupdata();
        System.out.println("Arshconstants" + AppConstants.cname + AppConstants.email + AppConstants.symptom1 + AppConstants.symptom2 + AppConstants.symptom3 + AppConstants.details + AppConstants.img + AppConstants.plan + AppConstants.price + AppConstants.plan_id);
        //  Toast.makeText(Plans.this, "payment id"+payment_id, Toast.LENGTH_SHORT).show();


    }


    private void buyViews() {


        StringRequest stringRequest = new StringRequest(Request.Method.POST, BUY_VIEWS_URL,
                new Response.Listener<String>() {

                    public void onResponse(String response) {
                        System.out.println("res= " + response);
                        //    System.out.println("reg"+GET_PLANS__URL.toString()+response.toString());
                        pdialog.dismiss();
                        try {
                            JSONObject j = new JSONObject(response);
                            System.out.println("resArsh" + response.toString() + response.toString());
                            res = j.getString("message");
                            AlertDialog.Builder builder = new AlertDialog.Builder(Plans.this);
                            builder.setMessage(res)
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {


                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();

                        } catch (JSONException e) {
                            System.out.println("jsonexeption1111" + e.toString());
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getApplicationContext(), "Please check your internet connection...!!!", Toast.LENGTH_LONG).show();
                        System.out.println("jsonexeption" + error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                try {

                    params.put(KEY_PAYMENT_ID, payment_id);
                    params.put(KEY_PACKAGE_NAME, AppConstants.plan);
                    params.put(KEY_AMOUNT_PAID, AppConstants.price);
                    params.put(KEY_PAYMENT_TXN_ID, AppConstants.txn_id);


                } catch (Exception e) {
                    System.out.println("arsherror" + e.toString());
                }


                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    /////////////////////////////////////


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("Arshconstantsbuyviews" + AppConstants.cname + AppConstants.email + AppConstants.symptom1 + AppConstants.symptom2 + AppConstants.symptom3 + AppConstants.details + AppConstants.img + AppConstants.plan + AppConstants.price + AppConstants.plan_id);

        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.i(TAG, confirm.toJSONObject().toString(4));
                        Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));
                        System.out.println("Arsh" + confirm.toJSONObject().toString(4).toString());//

                        JSONObject jo = new JSONObject(confirm.toJSONObject().toString(4));
                        String s = jo.getString("response");
                        JSONObject resid = new JSONObject(s);
                        String id = resid.getString("id");
                        AppConstants.txn_id = id.toString();

                        System.out.println("resPaypal" + s.toString() + "id  " + id);
                        System.out.println("Arsh1" + confirm.getPayment().toJSONObject().toString(4).toString());

                        // Booknow();

                        pdialog.setCancelable(true);
                        pdialog.setMessage("One Moment...");
                        pdialog.show();

                        buyViews();
                        System.out.println("Arshconstantsbuyviews" + AppConstants.plan + AppConstants.price + AppConstants.plan_id + AppConstants.cname + AppConstants.email + AppConstants.symptom1 + AppConstants.symptom2 + AppConstants.symptom3 + AppConstants.details + AppConstants.img);

                        // buyViews();
                        /**
                         *  TODO: send 'confirm' (and possibly confirm.getPayment() to your server for verification
                         * or consent completion.
                         * See https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                         * for more details.
                         *
                         * For sample mobile backend interactions, see
                         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
                         */
                        //  displayResultText("PaymentConfirmation info received from PayPal");

                        //  Booknow();

                     /*   Toast.makeText(getApplicationContext(), "Order placed",
                                Toast.LENGTH_LONG).show();*/
                      /*  AlertDialog.Builder builder = new AlertDialog.Builder(this);

                        builder.setTitle("Dry Cleaning")
                                .setMessage("The Order Has Been Placed Successfully")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //do things
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();*/
                    } catch (JSONException e) {
                        Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                        Toast.makeText(getApplicationContext(), "an extremely unlikely failure occurred:",
                                Toast.LENGTH_LONG).show();
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "Payment canceled.");
                Toast.makeText(getApplicationContext(), "Payment Cancelled By User.",
                        Toast.LENGTH_LONG).show();
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        TAG,
                        "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
                Toast.makeText(getApplicationContext(), "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.",
                        Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("FuturePaymentExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("FuturePaymentExample", authorization_code);

                        sendAuthorizationToServer(auth);
                        //   displayResultText("Future Payment code received from PayPal");

                    } catch (JSONException e) {
                        Log.e("FuturePaymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("FuturePaymentExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        "FuturePaymentExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        } else if (requestCode == REQUEST_CODE_PROFILE_SHARING) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalProfileSharingActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("ProfileSharingExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("ProfileSharingExample", authorization_code);

                        sendAuthorizationToServer(auth);
                        //   displayResultText("Profile Sharing code received from PayPal");

                    } catch (JSONException e) {
                        Log.e("ProfileSharingExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("ProfileSharingExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        "ProfileSharingExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
    }

    private void sendAuthorizationToServer(PayPalAuthorization authorization) {

        /**
         * TODO: Send the authorization response to your server, where it can
         * exchange the authorization code for OAuth access and refresh tokens.
         *
         * Your server must then store these tokens, so that your server code
         * can execute payments for this user in the future.
         *
         * A more complete example that includes the required app-server to
         * PayPal-server integration is available from
         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
         */

    }

       /* public void onFuturePaymentPurchasePressed(View pressed) {
            // Get the Client Metadata ID from the SDK
            String metadataId = PayPalConfiguration.getClientMetadataId(this);

            Log.i("FuturePaymentExample", "Client Metadata ID: " + metadataId);

            // TODO: Send metadataId and transaction details to your server for processing with
            // PayPal...
            displayResultText("Client Metadata Id received from SDK");
        }*/


    /////////////////////////////////////////////////////////


    //////////////////////////////////////
    private void showpickupdata() {

        System.out.println("Arshconstants" + AppConstants.cname + AppConstants.email + AppConstants.symptom1 + AppConstants.symptom2 + AppConstants.symptom3 + AppConstants.details + AppConstants.img);

        //showpDialog();
//        progress.setVisibility(View.VISIBLE);
        String urlJsonObj = "http://designer321.com/kaushaljohn/ayurvedic/api/get_all_packages.php";
        System.out.println("urljsonobject" + urlJsonObj);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                urlJsonObj, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                //   Log.d(TAG, response.toString());


                System.out.println("" + response.toString());

                try {

                    ///listmodel
                    mylist = new ArrayList<Views_price_list_item>();

                    res = response.getString("response");
                    System.out.println("res" + res);
                    JSONArray jarray = new JSONArray(res);


                    for (int i = 0; i < jarray.length(); i++) {
                        JSONObject jObj = jarray.getJSONObject(i);

//gv_plan_id,plan_name,views_count,fee
                        gv_plan_id = jObj.getString("package_id");
                        plan_name = jObj.getString("package_name");
                        fee = jObj.getString("package_fee");
                        //  fee = jObj.getString("fee");


                        //   System.out.println("video_id"+video_id.toString()+title_of_video.toString()+video_url.toString());

                        Views_price_list_item OneObject = new Views_price_list_item();

                        System.out.println("all order");
                        OneObject.setPackage_id(gv_plan_id);
                        OneObject.setPackage_name(plan_name);

                        OneObject.setPackage_fee(fee);

                        mylist.add(OneObject);


                        adapter = new ArrayAdapter<Views_price_list_item>(Plans.this,
                                R.layout.views_price_list_layout, mylist) {
                            myViewHolder holder = null;

                            @Override
                            public View getView(final int position, View convertView,
                                                ViewGroup parent) {
                                // TODO Auto-generated method stub

                                View row = convertView;


                                if (row == null) {


                                    row = Plans.this.getLayoutInflater().inflate(
                                            R.layout.views_price_list_layout, parent, false);


                                    holder = new myViewHolder(row);
                                    row.setTag(holder);
                                } else {
                                    holder = (myViewHolder) row.getTag();

                                }

                                Views_price_list_item d3 = new Views_price_list_item();
                                d3 = (Views_price_list_item) mylist.get(position);
                                //	holder.app_name.setText(d3.getviews_title_of_video.toString());
                                holder.cust_name.setText(d3.getPackage_name().toString());
                                holder.total_price.setText(" $ " + d3.getPackage_fee().toString());


                                listOne.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        System.out.println("Arplanpre" + price + plan);
                                        new AlertDialog.Builder(Plans.this, R.style.AlertDialogCustom)
                                                .setTitle("Ayurvedic App")
                                                .setMessage("Do you want to Submit the Information ?")
                                                .setIcon(R.mipmap.app_icon)
                                                .setPositiveButton("Submit", new DialogInterface.OnClickListener() {

                                                    public void onClick(DialogInterface dialog, int whichButton) {


                                                        buyPlans();
                                                    }
                                                })
                                                .setNegativeButton(android.R.string.no, null).show();

                                        Views_price_list_item d3 = new Views_price_list_item();
                                        d3 = (Views_price_list_item) mylist.get(position);


                                        plan = d3.getPackage_name().toString();
                                        price = d3.getPackage_fee().toString();
                                        plan_id = d3.getPackage_id().toString();

                                        AppConstants.plan = plan.toString();
                                        AppConstants.price = price.toString();
                                        AppConstants.plan_id = plan_id.toString();
                                        System.out.println("Arplanpost" + price + plan + plan_id + views_count);
                                        money = new BigDecimal(price);

                                    }
                                });

                                return row;
                            }
                        };


                        listOne.setAdapter(adapter);


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {


            public void onErrorResponse(VolleyError error) {
                // VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(Plans.this,
                        "Please Check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }

    class myViewHolder {
        //    TextView app_name;
        TextView cust_name;
        TextView total_price;

        //  TextView app_des;

        myViewHolder(View v) {
            //	app_name = (CheckBox) v.findViewById(R.id.checkBox4);
            cust_name = (TextView) v.findViewById(R.id.driver_show_pickup_list_customer_name);
            total_price = (TextView) v.findViewById(R.id.driver_show_pickup_list_total_price);

            //	app_quantity=(EditText)v.findViewById(R.id.editText6);


        }
    }

    ///////////////// Buy Plans ///////////////

    public void buyPlans() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, BUY_VIEWS_URL,
                new Response.Listener<String>() {

                    public void onResponse(String response) {
                        System.out.println("res= " + response);
                        //    System.out.println("reg"+GET_PLANS__URL.toString()+response.toString());
                        pdialog.dismiss();
                        try {
                            JSONObject j = new JSONObject(response);
                            System.out.println("resArsh" + response.toString() + response.toString());
                            res = j.getString("message");
                            AlertDialog.Builder builder = new AlertDialog.Builder(Plans.this, R.style.AlertDialogCustom);
                            builder.setMessage(res)
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            AppConstants.cname = "";
                                            AppConstants.email = "";
                                            AppConstants.symptom1 = "";
                                            AppConstants.symptom2 = "";
                                            AppConstants.symptom3 = "";
                                            AppConstants.details = "";
                                            AppConstants.additional_details = "";
                                            AppConstants.img = "";
                                            AppConstants.plan = "";
                                            AppConstants.price = "";
                                            AppConstants.plan_id = "";
                                            AppConstants.gender = "";
                                            AppConstants.age = "";



                                            Intent intent = new Intent(Plans.this, Activity_save_data.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                            finishAffinity();

                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();

                        } catch (JSONException e) {
                            System.out.println("jsonexeption1111" + e.toString());
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getApplicationContext(), "Please check your internet connection...!!!", Toast.LENGTH_LONG).show();
                        System.out.println("jsonexeption" + error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                try {

                    params.put(KEY_PAYMENT_ID, payment_id);
                    params.put(KEY_PACKAGE_NAME, AppConstants.plan);
                    params.put(KEY_AMOUNT_PAID, AppConstants.price);


                } catch (Exception e) {
                    System.out.println("arsherror" + e.toString());
                }


                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);


    }


    /////////////////////
/////////////
    private PayPalPayment getThingToBuy(String paymentIntent) {

        System.out.println("Arplan" + price + plan);
        return new PayPalPayment(new BigDecimal(price), "USD", plan,
                paymentIntent);
    }

    public void listViewPressed() {

        if (ConnectionDetection.isNetworkAvailable(Plans.this) == true) {

               /* Booknow();*/


            PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);

        /*
         * See getStuffToBuy(..) for examples of some available payment options.
         */

            Intent intent = new Intent(Plans.this, PaymentActivity.class);

            // send the same configuration for restart resiliency
            intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

            intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);

            startActivityForResult(intent, REQUEST_CODE_PAYMENT);
             /*   Booknowwithoutpayment();*/

            //   makeJsonArrayRequest();
        } else {
            Toast.makeText(getApplicationContext(), "Please Connect to internet", Toast.LENGTH_LONG).show();
        }


    }


    /////////////////////////////////////////////


    @Override
    public void onBackPressed() {

        Intent intent = new Intent(this, Activity_save_data.class);
        startActivity(intent);
        finish();
    }
}
