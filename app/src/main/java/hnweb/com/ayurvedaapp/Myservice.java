package hnweb.com.ayurvedaapp;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by Hnweb on 10/5/2016.
 */
public class Myservice extends Service {
    private final IBinder mBinder = new MyBinder();

    @Override
    public void onCreate() {
        super.onCreate();
        System.out.println("Service Create");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        System.out.println("Service Destroy");
    }

    @Override
    public boolean onUnbind(Intent intent) {

        System.out.println("Service unbind");
        return super.onUnbind(intent);

    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
        System.out.println("Service rebind");
    }

    @Nullable

    @Override
    public IBinder onBind(Intent intent) {
        System.out.println("bind");
        return null;

    }

    public class MyBinder extends Binder {
        Myservice getService() {
            return Myservice.this;
        }
    }


}
