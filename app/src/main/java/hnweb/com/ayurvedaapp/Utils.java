package hnweb.com.ayurvedaapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

/**
 * Created by Hnweb on 10/6/2016.
 */
public class Utils
{
    public static void showAlert(Context context,String message )

    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        AppConstants.logout_flag="0";
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void confirmLogoutAlert(Context context,String message )

    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Logout", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    //  showAlert(Activity_login.class,"Logout");

                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    public static ProgressDialog showProgressDialog(Context context, String text){
        ProgressDialog progress;

        progress=new ProgressDialog(context);
        progress.setMessage(text);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.show();
        return progress;
    }


}
