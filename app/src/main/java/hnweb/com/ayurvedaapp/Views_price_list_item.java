package hnweb.com.ayurvedaapp;

/**
 * Created by HNWeb-18 on 7/30/2016.
 */
public class Views_price_list_item {
    String package_id;
    String package_name;
    String package_fee;

    public String getPackage_id() {
        return package_id;
    }

    public void setPackage_id(String package_id) {
        this.package_id = package_id;
    }

    public String getPackage_name() {
        return package_name;
    }

    public void setPackage_name(String package_name) {
        this.package_name = package_name;
    }

    public String getPackage_fee() {
        return package_fee;
    }

    public void setPackage_fee(String package_fee) {
        this.package_fee = package_fee;
    }
}